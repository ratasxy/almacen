package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import domain.Warehouse;

@Entity
@Table(name = "room")
public class Room {
	
	@Id
	@SequenceGenerator(name = "room_id_generator", sequenceName = "room_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "room_id_generator")
	private int id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	@ManyToOne
	private Warehouse warehouse;
	
	@OneToMany(mappedBy = "room")
	private Collection<Row> rw;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Warehouse getWarehouse() {
		return warehouse;
	}
	
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	
}