package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ppcontainer")

public class ProviderProductContainer {
	
	@Id
	@SequenceGenerator(name = "ppcontainer_id_generator", sequenceName = "ppcontainer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ppcontainer_id_generator")
	private int id;
	
	@OneToMany(mappedBy = "productContainer")
	private Collection<ProductContainer> productContainer;
	
	@ManyToOne
	private Provider providerd;

}
