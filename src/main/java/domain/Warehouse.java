package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "warehouse")
public class Warehouse {
	
	@Id
	@SequenceGenerator(name = "warehouse_id_generator", sequenceName = "warehouse_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "warehouse_id_generator")
	private int id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double latitude;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double longitude;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double address;
	
	@OneToMany(mappedBy = "warehouse")
	private Collection<Room> wh;
	
	@ManyToOne
	private Persona persona;
	
	public Warehouse() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAddress() {
		return address;
	}

	public void setAddress(double address) {
		this.address = address;
	}
	
	@Override
	public String toString(){
		return "Warehouse: " + this.name + " ";
	}
	
}