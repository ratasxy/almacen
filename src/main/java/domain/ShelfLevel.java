package domain;

import domain.Shelf;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "shelfLevel")

public class ShelfLevel {
	
	@Id
	@SequenceGenerator(name = "shelfLevel_id_generator", sequenceName = "shelfLevel_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shelfLevel_id_generator")
	private int id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double height;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double width;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double depth;
	
	@ManyToOne
	private Shelf shelf;
	
	@ManyToOne
	private Kardex kardex;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Shelf getShelf() {
		return shelf;
	}
	
	public void setShelf(Shelf shelf) {
		this.shelf = shelf;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getDepth() {
		return depth;
	}
	
	public void setDepth(double depth) {
		this.depth = depth;
	}
	
	/*@Override
	public String toString(){
		return this.shelf + "ShelfLevel: " + this.name + " ";
	}*/
	

}
