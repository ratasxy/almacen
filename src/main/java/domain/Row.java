package domain;
import domain.Room;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "row")

public class Row {
	
	@Id
	@SequenceGenerator(name = "row_id_generator", sequenceName = "row_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "row_id_generator")
	private int id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	@ManyToOne
	private Room room; 
	
	@OneToMany(mappedBy = "row")
	private Collection<Shelf> sf;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Room getRoom() {
		return room;
	}
	
	public void setRoom(Room room) {
		this.room = room;
	}
	
	/*@Override
	public String toString(){
		return this.rw + "Row: " + this.name + " ";
	}*/

}
