package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "provider")
public class Provider {
	
	@Id
	@SequenceGenerator(name = "provider_id_generator", sequenceName = "provider_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_id_generator")
	private int id;
	
	@OneToMany(mappedBy = "provider")
	private Collection<Business> bss;
	
	@OneToMany(mappedBy = "providerd")
	private Collection<ProviderProductContainer> ppc;
	
	public void list_product(){
	}
	public boolean confirm(){
		return true;
	}
}
